
/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"
/* Exported functions ------------------------------------------------------- */

void Delay_Config(void) __attribute__ ((constructor));
void Delay_ms (uint32_t DelayTime);
void Delay_us (uint32_t DelayTime);



/* Includes ------------------------------------------------------------------*/
#include "Delay.h"
//#include "main.h"
/* Private functions ---------------------------------------------------------*/


/**
  * @brief  This function configures timer.
  * @param  None
  * @retval None
  */

void Delay_Config(void)
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
//	TIM_TimeBaseInitStruct.TIM_ClockDivision 			= TIM_CKD_DIV4;
	TIM_TimeBaseInitStruct.TIM_CounterMode 				= TIM_CounterMode_Up;
	TIM_TimeBaseInitStruct.TIM_Prescaler 					= 0;
	TIM_TimeBaseInitStruct.TIM_RepetitionCounter 	= 0;
	TIM_TimeBaseInitStruct.TIM_Period 						= 84-1;
	TIM_TimeBaseInit(TIM6, &TIM_TimeBaseInitStruct);
	TIM_Cmd(TIM6, ENABLE);
}
/**
  * @brief  This function creates delay in microsecond.
  * @param  Delay time in microsecond
  * @retval None
  */

void Delay_us(uint32_t DelayTime){
	TIM_ITConfig(TIM6, TIM_IT_Update, ENABLE);
	while (DelayTime){
		while (TIM_GetITStatus(TIM6, TIM_IT_Update) == RESET);
			TIM_ClearITPendingBit(TIM6, TIM_IT_Update);
			DelayTime--;	
	}
}

/**
  * @brief  This function creates delay in milisecond.
  * @param  Delay time in milisecond
  * @retval None
  */
void Delay_ms(uint32_t DelayTime){
	Delay_us(DelayTime*1000);
}


/************************ (C) COPYRIGHT VIAMLAB *****END OF FILE****/
